variable "project" {
  default = "xxxxxxx"
}

variable "region" {
  default = "asia-northeast1"
}

variable "zone" {
  default = "asia-northeast1-b"
}

variable "base-name" {
  default = "xxxxxxx"
}

variable "google_compute_subnetwork_ip_cidr_range" {
  default = "10.1.1.0/24"
}
