// instance grope設定
resource "google_compute_instance_group" "webservers" {
  name        = "${var.base-name}-webservers"
  description = "Terraform test instance group"

  instances = [
    google_compute_instance.default.self_link,
  ]

  named_port {
    name = "http"
    port = "80"
  }

  //  named_port {
  //    name = "https"
  //    port = "8443"
  //  }

  zone = var.zone
}

// GCE Instance 設定
resource "google_compute_instance" "default" {
  name         = "${var.base-name}-worker-1"
  machine_type = "f1-micro"
  zone         = var.zone

  tags = ["ssh", "front", "web"]

  boot_disk {
    initialize_params {
      image = "cos-cloud/cos-77-lts"
    }
  }

  // Local SSD disk
  //  scratch_disk {
  //   interface = "SCSI"
  //  }

  // custネットワークを指定
  network_interface {
    network    = google_compute_network.custom-network.self_link
    subnetwork = google_compute_subnetwork.custom-subnetwork.self_link

    access_config {
      // Ephemeral IP
    }
  }

  // custom metadata 設定
  metadata = {
    // startup scrit設定（nginx 起動)
    startup-script = <<EOF
#!/bin/bash

docker run --name nginx -d -p 80:80 nginx
EOF 

  }

  // すべてのscopeにアクセス可能にする
  service_account {
    scopes = ["cloud-platform"]
  }
}