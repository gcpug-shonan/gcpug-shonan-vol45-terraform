resource "google_compute_global_forwarding_rule" "default" {
  name       = "global-rule"
  target     = google_compute_target_http_proxy.default.self_link
  port_range = "80"
}

resource "google_compute_target_http_proxy" "default" {
  name        = "${var.base-name}-target-proxy"
  description = "a description"
  url_map     = google_compute_url_map.default.self_link
}

resource "google_compute_url_map" "default" {
  name            = "${var.base-name}-url-map-target-proxy"
  description     = "a description"
  default_service = google_compute_backend_service.default.self_link

}

resource "google_compute_backend_service" "default" {
  name        = "${var.base-name}-backend"
  port_name   = "http"
  protocol    = "HTTP"
  timeout_sec = 10

  health_checks = [google_compute_http_health_check.default.self_link]

  backend {
    group = google_compute_instance_group.webservers.self_link
  }
}

resource "google_compute_http_health_check" "default" {
  name               = "check-backend"
  request_path       = "/"
  check_interval_sec = 1
  timeout_sec        = 1
}



