// subnetwork 設定
resource "google_compute_subnetwork" "custom-subnetwork" {
  name          = "${var.base-name}-subnetwork"
  ip_cidr_range = var.google_compute_subnetwork_ip_cidr_range
  region        = var.region
  network       = google_compute_network.custom-network.self_link
}

// network 設定
resource "google_compute_network" "custom-network" {
  name                    = "${var.base-name}-network"
  auto_create_subnetworks = false
}

// firewall 設定
resource "google_compute_firewall" "custom-ssh-allow" {
  name    = "${var.base-name}-ssh-allow"
  network = google_compute_network.custom-network.name


  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]

  target_tags = ["ssh"]
}

resource "google_compute_firewall" "custom-icmp-allow" {
  name    = "${var.base-name}-icmp-allow"
  network = google_compute_network.custom-network.name

  allow {
    protocol = "icmp"
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "custom-http-allow" {
  name    = "${var.base-name}-http-allow"
  network = google_compute_network.custom-network.name

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = ["0.0.0.0/0"]

  target_tags = ["front"]
}

// VM間でアクセス許可
resource "google_compute_firewall" "custom-internal-allow" {
  name    = "${var.base-name}-internal-allow"
  network = google_compute_network.custom-network.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["0-65535"]
  }

  source_ranges = ["10.128.0.0/9"]
}

resource "google_compute_firewall" "custom-lb-allow" {
  name    = "${var.base-name}-lb-allow"
  network = google_compute_network.custom-network.name

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = ["130.211.0.0/22", "35.191.0.0/16"]
  target_tags   = ["web"]
}