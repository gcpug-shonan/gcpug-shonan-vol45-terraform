provider "google" {
  //credentials = file("xxxxx.json")
  project = var.project
  region  = var.region
  zone    = var.zone
}

# terraform {
#   backend "gcs" {
#     bucket = "xxxx"
#   }
# }

# resource "google_storage_bucket" "terraform-state-store" {
#   name          = "fg-infra-terraform-terraform-stete"
#   location      = var.region
#   storage_class = "REGIONAL"

#   versioning {
#     enabled = true
#   }

#   lifecycle_rule {
#     action {
#       type = "Delete"
#     }
#     condition {
#       num_newer_versions = 5
#     }
#   }
# }