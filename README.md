# gcpug-shonan-vol45-terraform

GCPUG Shonan Vol.45 で利用したインフラ構築用terraformです。

完成形ではなく、これをベースに参加者と話をしながら試していくためのベースプロジェクトです。

## 準備

###  認証の設定

以下を実行するとローカルの自分のアカウントで実行することができます。

```
gcloud auth application-default login
```

### 各種設定

`variables.tf`  の `xxxxx` の部分を自分の設定に書き換えて下さい。


## terraformの実行

事前確認
```
terraform plan
```

実行
```
terraform apply
```

削除
```
terraform destroy
```
